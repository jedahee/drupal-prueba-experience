<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/userslist/templates/users-list-template.html.twig */
class __TwigTemplate_341b4718e79275801c0dec2a69a2f0a7026d5f9cb8140e55e1202d30fab553f9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "
<!-- ESTAS LÍNEAS HACE QUE PODAMOS USAR CADA CAMPO POR SEPARADO -->
";
        // line 4
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "form_build_id", [], "any", false, false, true, 4), 4, $this->source), "html", null, true);
        echo "

";
        // line 6
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "form_token", [], "any", false, false, true, 6), 6, $this->source), "html", null, true);
        echo "

";
        // line 8
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "form_id", [], "any", false, false, true, 8), 8, $this->source), "html", null, true);
        echo "

<!-- FORMULARIO DE FILTRO -->
<nav class=\"filter\">
    
    ";
        // line 13
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, true, 13), 13, $this->source), "html", null, true);
        echo "
    ";
        // line 14
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "surname1", [], "any", false, false, true, 14), 14, $this->source), "html", null, true);
        echo "
    ";
        // line 15
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "surname2", [], "any", false, false, true, 15), 15, $this->source), "html", null, true);
        echo "
    ";
        // line 16
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", false, false, true, 16), 16, $this->source), "html", null, true);
        echo "
    ";
        // line 17
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "username", [], "any", false, false, true, 17), 17, $this->source), "html", null, true);
        echo "
    ";
        // line 18
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "actions", [], "any", false, false, true, 18), "submit", [], "any", false, false, true, 18), 18, $this->source), "html", null, true);
        echo "

</nav>


<main class=\"content\">
    
    <!-- TABLA DE USUARIOS -->
    <table class=\"users-table\">
        <!-- CABECERA -->
        <thead>
            <th>Nombre</th>
            <th>Nombre de usuario</th>
            <th>Primer apellido</th>
            <th>Segundo apellido</th>
            <th>Correo electrónico</th>
            
        </thead>
        
        <tbody id=\"tbody_content\">

            <!-- RELLENANDO FILAS DE LA TABLA CON LOS DATOS DEL USUARIO -->
            ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((($__internal_compile_0 = ($context["form"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0["#users"] ?? null) : null));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 41
            echo "            <tr>
                <td>";
            // line 42
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["user"], "name", [], "any", false, false, true, 42), 42, $this->source), "html", null, true);
            echo "</td>
                <td>";
            // line 43
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["user"], "username", [], "any", false, false, true, 43), 43, $this->source), "html", null, true);
            echo "</td>
                <td>";
            // line 44
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["user"], "surname1", [], "any", false, false, true, 44), 44, $this->source), "html", null, true);
            echo "</td>
                <td>";
            // line 45
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["user"], "surname2", [], "any", false, false, true, 45), 45, $this->source), "html", null, true);
            echo "</td>
                <td>";
            // line 46
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["user"], "email", [], "any", false, false, true, 46), 46, $this->source), "html", null, true);
            echo "</td>
            </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "        </tbody>
        
        <tfoot>
            <!-- PAGINADOR -->
            <div class=\"paginator\" id=\"paginator\">
                ";
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((($__internal_compile_1 = ($context["form"] ?? null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1["#paged"] ?? null) : null));
        foreach ($context['_seq'] as $context["_key"] => $context["num"]) {
            // line 55
            echo "                    <p>";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["num"], 55, $this->source), "html", null, true);
            echo "</p>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['num'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "            </div>
        </tfoot>
    </table>
    
    <div class=\"response\">
        <!-- RESPUESTA CUANDO NO SE ENCUENTRA NINGÚN USUARIO EN EL FILTRO -->
    </div>
    <!-- FIN TABLA DE USUARIOS -->
</main>";
    }

    public function getTemplateName()
    {
        return "modules/custom/userslist/templates/users-list-template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 57,  149 => 55,  145 => 54,  138 => 49,  129 => 46,  125 => 45,  121 => 44,  117 => 43,  113 => 42,  110 => 41,  106 => 40,  81 => 18,  77 => 17,  73 => 16,  69 => 15,  65 => 14,  61 => 13,  53 => 8,  48 => 6,  43 => 4,  39 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/userslist/templates/users-list-template.html.twig", "C:\\xampp\\htdocs\\drupal_prueba_eXperience\\modules\\custom\\userslist\\templates\\users-list-template.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("for" => 40);
        static $filters = array("escape" => 4);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['for'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
