<?php

namespace Drupal\userslist\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\RemoveCommand;

/**
 * Formulario para filtrar en la plantilla users-list-template.
 */
class userslistForm extends FormBase {

  public function getFormId() { // Id del formulario
    return 'userslist_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    
    // Campos del formulario con sus atributos
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombre'),
      '#required' => FALSE,
      '#maxlength' => 20,
      '#default_value' => '',
    ];
	  $form['surname1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Primer apellido'),
      '#required' => FALSE,
      '#maxlength' => 20,
      '#default_value' => '',
    ];
    $form['surname2'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Segundo Apellido'),
        '#required' => FALSE,
        '#maxlength' => 20,
        '#default_value' => '',
      ];
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombre de usuario'),
      '#required' => FALSE,
      '#maxlength' => 20,
      '#default_value' => '',
    ];
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Correo electrónico'),
      '#required' => FALSE,
      '#maxlength' => 20,
      '#default_value' => '',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'button',
      '#button_type' => 'primary',
      '#value' => $this->t('Filtrar'),
      '#ajax' => [ // Botón AJAX
        'callback' => '::filterUsers'
      ]
    ];

    // Acciones que se realizan cuando la página carha

    $args = $this->getData(); // Obtengo los usuarios y la paginación
    $users = $args['users']; // Usuarios
    $paged = $args['paged']; // Paginación
    
    // Pasando variables al formulario
    $form['#paged'] = $paged;
    $form['#users'] = $users;   

    $form['#theme'] = 'users_list_template'; // Plantilla del tema
    $form['#attached']['library'] = 'userslist/userslist'; // Para vincular el CSS y el JS con el formulario
    return $form;
  }

  // No hay validaciones...
  public function validateForm(array & $form, FormStateInterface $form_state) { }

  // Las peticiones se hacen con AJAX, por lo cual esta función no hace nada
  public function submitForm(array & $form, FormStateInterface $form_state) { }

  // Función para filtrar usuarios
  public function filterUsers(array & $form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse(); // Variable AJAX a devolver
    
    $users = $this->getAllUsers(); // Obteniendo todos los usuarios

    // Variables
    $paged_arr = [];
    $users_filtered = []; 
    
    // Variables con el valor de los campos del formulario
    $name = $form_state->getValue('name');
    $surname1 = $form_state->getValue('surname1');
    $surname2 = $form_state->getValue('surname2');
    $username = $form_state->getValue('username');
    $email = $form_state->getValue('email');
    
    // Comparamos que los campos no estén vacios
    if ($name!="" || $surname1!="" || $surname2!="" || $username!="" || $email!="") {
      foreach ($users as $user){

        // Filtamos el usuario (si coincide alguno de los campos, se añade el usuario con formato HTML al array)
        if ($name == $user->name ||
            $surname1 == $user->surname1 ||
            $surname2 == $user->surname2 ||
            $username == $user->username ||
            $email == $user->email) {
              $users_filtered[] = "<tr><td>". $user->name ."</td><td>". $user->username ."</td><td>". $user->surname1 ."</td><td>" . $user->surname2 . "</td><td>" . $user->email . "</td></tr>";
        }
      }

      // Si no se han encontrado usuarios. Mostramos el mensaje de error
      if (count($users_filtered) <= 0) 
        $ajax_response->addCommand(new HtmlCommand('.response', "<p>No se ha encontrado ningún usuario. Filtra con todos los campos vacios para mostrar todos los usuarios de nuevo.</p>"));
      else 
        $ajax_response->addCommand(new RemoveCommand('.response > p')); // Si no es así, lo borramos
      
      // Estableciendo variables (con contenido HTML personalizado) a un contenedor HTML
      $ajax_response->addCommand(new HtmlCommand('#tbody_content', implode(" ", $users_filtered)));
      $ajax_response->addCommand(new HtmlCommand('#paginator', implode(" ", $paged_arr)));

  
    } else {
      $args = $this->getData(); // Obtengo los usuarios y la paginación
      $users = $args["users"]; // Usuarios
      $paged = $args["paged"]; // Paginación
      
      // Variables
      $paged_formatted = [];
      $users_html = [];

      // Formateando usuarios con HTML 
      foreach ($users as $user)
        $users_html[] = "<tr><td>". $user->name ."</td><td>". $user->username ."</td><td>". $user->surname1 ."</td><td>" . $user->surname2 . "</td><td>" . $user->email . "</td></tr>";
      
      // Formateando números de página en HTML
      foreach($paged as $num_p) 
        $paged_formatted[] = "<p>". $num_p ."</p>";
      
      // Si no se han encontrado usuarios. Mostramos el mensaje de error 
      if (count($users) <= 0) 
        $ajax_response->addCommand(new HtmlCommand('.response', "<p>No se ha encontrado ningún usuario. Filtra con todos los campos vacios para mostrar todos los usuarios de nuevo.</p>"));
      else 
        $ajax_response->addCommand(new RemoveCommand('.response > p'));

      // Estableciendo variables (con contenido HTML personalizado) a un contenedor HTML
      $ajax_response->addCommand(new HtmlCommand('#tbody_content', implode(" ", $users_html)));
      $ajax_response->addCommand(new HtmlCommand('#paginator', implode(" ", $paged_formatted)));
    }
    
    return $ajax_response;
  }

  // Devuelve todos los usuarios obtenidos por la BBDD (users.json)
  public function getAllUsers() {
    $data = file_get_contents('http://localhost/drupal_prueba_eXperience/modules/custom/userslist/db/users.json'); // Obteniendo todos los usuarios
    $users = json_decode($data)->usuarios; // Lo transformamos a un array de PHP

    return $users;
  }

  // Esta función divide los usuarios en páginas de 5. Devuelve los usuarios y las páginas
  public function paginateUsers($users, $page) {
    $limit = 5;
    $limit_end = $limit * $page;
    $paged_no = ceil(count($users) / $limit); 

    $paged_arr = [];

    for ($i = 0; $i < $paged_no; $i++)
        array_push($paged_arr, ($i+1));
    

    if (count($users) > $limit) 
        $users_page = array_slice($users, ($limit_end-$limit), $limit_end);
    
    return [
      "users"=>$users_page,
      "paged"=>$paged_arr
    ];
  }

  // Obteniede los usuarios y paginación y los devuelve 
  public function getData($page = 1) {
    
    $users = $this->getAllUsers();

    $data = $this->paginateUsers($users, $page);
    $users = $data["users"];
    $paged = $data["paged"];

    return [
        'users' => $users, 
        'paged' => $paged
    ];
  }

}