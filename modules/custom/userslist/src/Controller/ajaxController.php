<?php


namespace Drupal\userslist\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class ajaxController extends ControllerBase {

  // Obtiene los usuarios dependiendo de la paginación pasada por parámetro (Esta función es la que se llama desde el userslist/js/script.js)  
  public function getUsers(Request $request) {
    
    // Obteniendo datos del JSON
    $data = file_get_contents('http://localhost/drupal_prueba_eXperience/modules/custom/userslist/db/users.json'); // Obteniendo todos los usuarios
    $users = json_decode($data)->usuarios; // Lo transformamos a un array de PHP
    
    $limit = 5;
    $num_page = \Drupal::request()->get('page'); // Obtenemos variable pasado por AJAX
    $limit_end = $limit * $num_page;

     
    $users_page = array_slice($users, ($limit_end-$limit), $limit_end);


    return new JsonResponse($users_page); // Devolviendo en formato JSON
  }

}
