
(function ($, Drupal) { // Cuando los elementos HTML están cargados 

  'use strict'; // Utilizar JS de forma estricta

  Drupal.behaviors.getusers = {  
    attach: function (context, settings) { 

        $(".paginator > p", context).once().each(function(e) { // Hacemos un bucle recorriendo todos los enlaces del paginador
            $(this).click(function(e) { // Evento CLICK

                var val_p = $(this).text(); // Obtenemos el texto (ej.: 1, 2, 3...)

                // Petición AJAX
                $.ajax({
                    url: Drupal.url('get-users'), // URL (definido en userslist.routing.yml)
                    type: 'POST',
                    data: {page: val_p}, // Pasando datos (valor de la paginación clickada)
                    dataType: 'json',
                    success: function (users) {
                        // Si todo va bien...

                        $("#tbody_content").empty(); // Vaciamos cuerpo de la tabla HTML de usuarios
                        
                        // Recorremos array de usuarios
                        users.forEach(user => {
                            let tr = $("<tr></tr>"); // Creamos fila...

                            // Creamos un campo para esta fila con los datos del usuario...
                            let td_name = $("<td>"+ user.name +"</td>")
                            let td_surname1 = $("<td>"+ user.surname1 +"</td>")
                            let td_surname2 = $("<td>"+ user.surname2 +"</td>")
                            let td_username = $("<td>"+ user.username +"</td>")
                            let td_email = $("<td>"+ user.email +"</td>")

                            // Añadimos los campos a la fila
                            tr.append(td_name);
                            tr.append(td_username);
                            tr.append(td_surname1);
                            tr.append(td_surname2);
                            tr.append(td_email);

                            // Añadimos la fila al cuerpo de la tabla HTML de usuarios
                            $("#tbody_content").append(tr);
                        });
                        
                    }
                });
            });
        });  
    }
  
  };

})(jQuery, Drupal); // Dependencias requeridas en el archivo...