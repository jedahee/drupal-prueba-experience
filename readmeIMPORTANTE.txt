# Versionado

Versión PHP: 8.0.25
Versión Drupal: 9.5
Versión Composer: 2.6.5

------------------

# Enlaces

URL de Gitlab: https://gitlab.com/jedahee/drupal-prueba-experience
URL del módulo personalizado en local: http://localhost/drupal_prueba_eXperience/userslist

------------------

# Info. del sitio

Titulo del sitio: Usuarios eXperience
Nombre de usuario: admin
Contraseña del sitio (usuario Owner): ijnLIDeo)Fl@r^9vU*
Correo electrónico vinculado a la web: jdaza.her@gmail.com
Visibilidad en los motores de búsqueda: No indexado

-------------------

# Info. extra

El proyecto ha sido creado con Composer (Composer init y Composer install/update)

No se ha utilizado drush para la gestión del proyecto

He utilizado y complementado AJAX con JS y sin JS para mostrar que tengo los conocimientos
para enviar peticiones AJAX desde JS/jQuery y desde Drupal Form.

Gracias por la atención :)
